<?php
switch ($action) {
    case 'accueil':
    {
        $message="Ce site permet d'enregistrer les participants à une épreuve.";
        include("views/v_accueil.php");
        break;
    }
    case 'lister': {
        $les_membres=$pdo->getLesMembres();
        require 'views/v_listemembres.php';
        break;
    }
    case 'saisir':
    {
        require "views/v_saisie.php";
        break;
    }

    case 'connexion':
    {
            require "views/v_connexion.php";
            break;
    }

    case 'supprimermembre':
{ 
    //AFFICHAGE DES MEMBRES
        $les_membres=$pdo->getLesMembres();
            require "views/v_supprimermembre.php";     
            break;
    }

    case 'controlesupprimermembre':
        {
            //JE RECUPERE LA VARIABLE 
            $id = $_REQUEST['id'];          

         // J'APPELLE LA FONCTION SUPPRESSION
                $resultat = $pdo->supprimermembre($id);
                
            //REAFFICHAGE DES MEMBRES
                $les_membres=$pdo->getLesMembres();
                include("views/v_listemembres.php");
            
        break;
        }
    


    case 'controlesaisie':
    {
        
        $nom = $_REQUEST['nom'];
        $prenom = $_REQUEST['prenom'];

        if (empty($nom) || empty($prenom)) {
            require "views/v_saisie.php";
        } else {
            $resultat = $pdo->insertMembre($nom, $prenom);
            $message = "Le nouveau membre $nom $prenom a bien été enregistrer";
            include("views/v_accueil.php");
        }
        break;
    }
    default:
    {
        $_SESSION["message_erreur"] = "Site en construction";
        include("views/404.php");
        break;
    }


}
